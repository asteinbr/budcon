#-*- coding: utf-8 -*-

class Context:
    def __init__(self, repository):
        self.__repository__ = repository
        self.__purchases__ = self.__repository__.getAllPurchases()
        self.__cluster_data__ = self.__repository__.getClusterData()
        self.__payment_methods__ = self.__repository__.getPaymentMethods()
        self.__vendors__ = self.__repository__.getVendors()


    def addPurchase(self, p):
        self.__repository__.addPurchase(p)
        self.__purchases__.append(p)
        if p.payment_method not in self.__payment_methods__:
            self.__payment_methods__.append(p.payment_method)
        if p.vendor not in self.__vendors__:
            self.__vendors__.append(p.vendor)

    def filterPurchases(self,pred):
        return filter(pred, self.__purchases__)

    def allPurchases(self):
        return self.__purchases__

    def getClusterEntry(self, vendor):
        if vendor in self.__cluster_data__:
            return self.__cluster_data__[vendor]
        else:
            return ''

    def addClusterEntry(self, vendor, type):
        if vendor not in self.__cluster_data__:
            self.__cluster_data__[vendor] = type

    def getClusterEntries(self):
        return self.__cluster_data__.itervalues()

    def getPaymentMethods(self):
        return self.__payment_methods__

    def getVendors(self):
        return self.__vendors__

