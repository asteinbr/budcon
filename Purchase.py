#-*- coding: utf-8 -*-
from datetime import datetime


class Purchase:
    def __init__(self, price, description, vendor, payment_method):
        self.description = description
        self.price = price
        self.vendor = vendor
        self.payment_method = payment_method
        #TODO: Wenn am Wochenende -> Auf nächsten Werktag verschieben
        self.date = datetime.now()

    def __str__(self):
        return str(self.description) + ' ' + str(self.price) + ' ' + str(self.vendor) + ' ' + str(self.payment_method) + ' ' + str(self.date)



        