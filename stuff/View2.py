#-*- coding: utf-8 -*-
__author__ = 'asteinbr'

import Tkinter as tk
from Tkinter import *

from Context import Context
from Repository import Repository

class View:
    def __init__(self, parent, context):
        self.__context__ = context
        self.myParent = parent

        # Buttonbar
        self.containerButtonbar = Frame(parent)
        self.containerButtonbar.pack()

        ## Button Add
        self.buttonAdd = Button(self.containerButtonbar, text="Hinzufügen", command=self.onActionButtonAdd).grid(row=0, column=0)

        ## Spacer
        tk.Label(self.containerButtonbar, text="\t\t\t\t\t\t\t").grid(row=0, column=1)

        ## Button Close
        self.buttonClose = Button(self.containerButtonbar, text="Schließen", command=self.onActionButtonClose).grid(row=0, column=5)

        ## Spacer
        self.containerSpacer = Frame(parent)
        self.containerSpacer.pack()
        self.createSpacerLabel()

        ## Table
        self.containerTable = Frame(parent)
        self.containerTable.pack()
        self.createTable()

    def createTable(self):
        i = 3
        bw = 2

        # Grid Header
        tk.Label(self.containerTable, text="Beschreibung", font=("Plain", 10, "bold underline"),
                      borderwidth=bw).grid(row=2, column=0)
        tk.Label(self.containerTable, text="Preis", font=("Plain", 10, "bold underline"),
                      borderwidth=bw).grid(row=2, column=1)
        tk.Label(self.containerTable, text="Datum", font=("Plain", 10, "bold underline"),
                      borderwidth=bw).grid(row=2, column=2)
        tk.Label(self.containerTable, text="Zahlungsart", font=("Plain", 10, "bold underline"),
                      borderwidth=bw).grid(row=2, column=3)
        tk.Label(self.containerTable, text="Verkäufer", font=("Plain", 10, "bold underline"),
                      borderwidth=bw).grid(row=2, column=4)
        tk.Label(self.containerTable, text="Cluster", font=("Plain", 10, "bold underline"),
                      borderwidth=bw).grid(row=2, column=5)

        # Spacer
        tk.Label(self.containerTable, text=" ", borderwidth=bw).grid(row=1, column=0)

        for p in self.__context__.allPurchases():
            tk.Label(self.containerTable, text=p.description,
                          borderwidth=bw).grid(row=i, column=0)
            tk.Label(self.containerTable, text=p.price,
                          borderwidth=bw).grid(row=i, column=1)
            tk.Label(self.containerTable, text=p.date,
                          borderwidth=bw).grid(row=i, column=2)
            tk.Label(self.containerTable, text=p.payment_method,
                          borderwidth=bw).grid(row=i, column=3)
            tk.Label(self.containerTable, text=p.vendor,
                          borderwidth=bw).grid(row=i, column=4)
            tk.Label(self.containerTable, text= self.__context__.getClusterEntry(p.vendor),
                          borderwidth=bw).grid(row=i, column=5)
            i += 1

    def onActionButtonAdd(self):
        print "...open add dialog"
        self.openAddDialog()
        #if self.buttonAdd["background"] == "green":
        #    self.buttonAdd["background"] = "yellow"
        #else:
        #    self.buttonAdd["background"] = "green"

    def onActionButtonClose(self):
        print "...destroy parent frame"
        self.myParent.destroy()

    def createSpacerLabel(self):
        Label(self.containerSpacer, text="bla")

    def openAddDialog(self):
        addDialog = Tk()
        addDialog.title = "Hinzufügen"
        addDialog.geometry("300x200")

        # Description
        tk.Label(addDialog, text="Beschreibung:").grid(row=0, column=0)
        entryDescription = tk.Entry(addDialog).grid(row=0, column=1)

        # Price
        tk.Label(addDialog, text="Preis:").grid(row=1, column=0)
        entryPrice = tk.Entry(addDialog).grid(row=1, column=1)

        # Payment Methods
        tk.Label(addDialog, text="Zahlungsart:").grid(row=2, column=0)

        varPaymentMethod = StringVar()
        listPaymentMethods = self.__context__.getPaymentMethods()
        optPaymentMethod = OptionMenu(addDialog, varPaymentMethod,
                                       *listPaymentMethods).grid(row=2, column=1)
        #optPaymentMethod.pack(fill=X)

        # Vendor
        tk.Label(addDialog, text="Verkäufer:").grid(row=3, column=0)

        varVendor = StringVar()
        listVendors = self.__context__.getVendors()
        optVendor = OptionMenu(addDialog, varVendor,
                                       *listVendors).grid(row=3, column=1)
        #optVendor.pack(fill=X)

        # Cluster
        tk.Label(addDialog, text="Cluster:").grid(row=4, column=0)

        varCluster = StringVar()
        listClusters = self.__context__.getClusterEntries()
        optCluster = OptionMenu(addDialog, varCluster,
                                       *listClusters).grid(row=4, column=1)
        #optCluster.pack(fill=X)

        # Cancel Button
        buttonCancel = Button(addDialog, text="Abbrechen",
                              command=self.onActionButtonCancelReciept).grid(row=5, column=0)

        # Add Button
        buttonAddReciept = Button(addDialog, text="Hinzufügen",
                                  command=self.onActionButtonAddReciept).grid(row=5, column=1)

    def state(self):
        print var1.get()

    def onActionButtonAddReciept(self):
        print "...adding reciept"

    def onActionButtonCancelReciept(self):
        print "...cancel add reciept"

            
# Vars
root = Tk()
root.title("BudCon")

repo = Repository('../database')
c = Context(repo)

app = View(root, c)
root.mainloop()