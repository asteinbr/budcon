

def typOfTriangle(l1, l2, l3):
    """
    >>> typOfTriangle('bla','bla','bla')
    'Keine Ahnung'
    >>> typOfTriangle('bla',2,2)
    'Keine Ahnung'
    >>> typOfTriangle(2,'bla',2)
    'Keine Ahnung'
    >>> typOfTriangle(2,2,'bla')
    'Keine Ahnung'
    >>> typOfTriangle(3,2,2)
    'Gleichschenklig'
    >>> typOfTriangle(2,3,2)
    'Gleichschenklig'
    >>> typOfTriangle(2,2,3)
    'Gleichschenklig'
    >>> typOfTriangle(3,3,3)
    'Gleichseitig'
    >>> typOfTriangle(1,2,3)
    'Ungleichseitig'
    >>> typOfTriangle(3.5,2.5,2.5)
    'Gleichschenklig'
    >>> typOfTriangle(2.5,3.5,2.5)
    'Gleichschenklig'
    >>> typOfTriangle(2.5,2.5,3.5)
    'Gleichschenklig'
    >>> typOfTriangle(3.5,3.5,3.5)
    'Gleichseitig'
    >>> typOfTriangle(1.5,2.5,3.5)
    'Ungleichseitig'
    """
    if not(isinstance(l1,int) or (isinstance(l1, float))) or (not(isinstance(l2,int) or (isinstance(l2, float))) or (not(isinstance(l3,int) or (isinstance(l3, float))))):
        return "Keine Ahnung"
    if l1 == l2  and  l2 == l3:
        return "Gleichseitig"
    if len(set([l1,l2,l3])) == 2:
        return "Gleichschenklig"
    return "Ungleichseitig"


if __name__ == "__main__":
    import doctest
    doctest.testmod()

