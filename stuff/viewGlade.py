#!/usr/bin/env python
#-*- coding: utf-8 -*-

import gtk

class BudCon(object):
    def __init__(self):
        self.builder = gtk.Builder()
        self.builder.add_from_file("gui.ui")
        self.builder.connect_signals(self)

        self.win =  self.builder.get_object('window1')
        self.about = self.builder.get_object('aboutdialog1')

        self.liststore = gtk.ListStore(str, str)
        

        self.path = None

    def run(self):
        try:
            gtk.main()
        except KeyboardInterrupt:
            pass
    
    def quit(self):
        gtk.main_quit()

    def ask_for_filename(self, title, default=None):
        dlg = gtk.FileChooserDialog(title=title,
                                    parent=self.win, 
                                    buttons=(gtk.STOCK_CANCEL,
                                             gtk.RESPONSE_REJECT,
                                             gtk.STOCK_OK,
                                             gtk.RESPONSE_OK))

        if default is not None:
            dlg.set_filename(default)

        result = dlg.run()

        if result == gtk.RESPONSE_OK:
            path = dlg.get_filename()
        else:
            path = None

        dlg.destroy()
        return path


    def info_msg(self, msg):
        dlg = gtk.MessageDialog(parent=self.win, 
                                type=gtk.MESSAGE_INFO, 
                                buttons=gtk.BUTTONS_OK,
                                message_format=msg
                                )
        dlg.run()
        dlg.destroy()

### Events
# Main Window
    def on_window1_delete_event(self, *args):
        self.quit()

# Actions
    def on_act_quit_activate(self, *args):
        self.quit()

    def onActionAboutDialog(self, *args):
        #self.info_msg('© BudCon-Team')
        self.about.show_all()

    def on_act_info_activate(self, *args):
        self.info_msg('© BudCon-Team')
    #    self.about.show_all()

    def on_act_delete_activate(self, *args): 
        self.info_msg('Delete')

    def on_act_paste_activate(self, *args):
        self.info_msg('Paste')

    def on_act_copy_activate(self, *args):
        self.info_msg('Copy')

    def on_act_cut_activate(self, *args):
        self.info_msg('Cut')

    def on_act_save_activate(self, *args):
        self.info_msg('Save')

    def on_act_new_activate(self, *args):
        print "...call on_act_new_activate"
        self.info_msg('New')
 
    def on_act_save_as_activate(self, *args):
        path = self.ask_for_filename('Open', self.path)
        if path is None:
            self.info_msg('File not saved.')
        else:
            self.path = path
            self.info_msg('File "%s" saved.'%self.path)

    def on_act_open_activate(self, *args):
        path = self.ask_for_filename('Open file...', self.path)
        if path is None:
            self.info_msg('No file chosen.')
        else:
            self.path = path
            self.info_msg('File "%s" chosen.'%self.path)


if __name__ == '__main__':
    app = BudCon()
    app.run()