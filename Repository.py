#-*- coding: utf-8 -*-
import sqlite3
from Purchase import Purchase


class Repository:
    def __init__(self, filename):
        self.conn = sqlite3.connect(filename)
        self.conn.text_factory = str

    def getAllPurchases(self):
        cursor = self.conn.cursor()
        purchases = []
        cursor.execute("SELECT Price, Description, VendorName, PaymentMethodName,  Date FROM AllPurchases")
        for row in cursor:
            p = Purchase(row[0],row[1],row[2], row[3])
            p.date = row[4]
            purchases.append(p)
        cursor.close()
        return purchases

    def addPurchase(self, p):
        vendorId = self.__get_vendor_key(p.vendor)
        payment_methodId = self.__get_payment_method_key(p.payment_method)
        cursor = self.conn.cursor()

        cursor.execute('insert into Purchases (Description, Date, VendorID , PaymentMethodID, Price) values (?,?,?,?,?)',[p.description, p.date, vendorId, payment_methodId, p.price])
        print 'Inserted: ', cursor.lastrowid
        self.conn.commit()
        cursor.close()

    def addClusterPair(self,vendor,type):
        vendorId = self.__get_vendor_key(vendor)
        clusterID = self.__get_cluster_key(type)
        cursor = self.conn.cursor()
        cursor.execute("Update Vendors Set ClusterID = :c_id where Vendors.ID = :v_id", {"c_id": clusterID, "v_id": vendorId})
        self.conn.commit()
        cursor.close()

    def getPaymentMethods(self):
        cursor = self.conn.cursor()
        payment_methods = []
        cursor.execute("SELECT Name FROM PaymentMethods")
        for r in cursor:
            payment_methods.append(r[0])
        cursor.close()
        return payment_methods

    def getVendors(self):
        cursor = self.conn.cursor()
        vendors = []
        cursor.execute("SELECT Name FROM Vendors")
        for r in cursor:
            vendors.append(r[0])
        cursor.close()
        return vendors

    def getClusterData(self):
        cursor = self.conn.cursor()
        clusterData = dict()
        cursor.execute("SELECT VendorName, ClusterName FROM Clusters")
        for row in cursor:
            clusterData[row[0]] = row[1]

        cursor.close()
        return clusterData

    def __get_payment_method_key(self, payment_method):
        id = None
        cursor = self.conn.cursor()
        cursor.execute("Select ID from PaymentMethods where Name = :p_m", {"p_m": payment_method})

        r = cursor.fetchone()
        if r == None:
            cursor.execute('insert into PaymentMethods (Name) values (?)',[payment_method]);
            id = cursor.lastrowid
        else:
            id = r[0]
        self.conn.commit()
        cursor.close()
        return id

    def __get_vendor_key(self, vendor):
        id = None
        cursor = self.conn.cursor()
        cursor.execute("Select ID from Vendors where Name = :v", {"v": vendor})

        r = cursor.fetchone()
        if r == None:
            cursor.execute('insert into Vendors (Name) values (?)',[vendor]);
            id = cursor.lastrowid
        else:
            id = r[0]
        self.conn.commit()
        cursor.close()
        return id

    def __get_cluster_key(self, clusterName):
        id = None
        cursor = self.conn.cursor()
        cursor.execute("Select ID from Cluster where Name = :c", {"c": clusterName})

        r = cursor.fetchone()
        if r == None:
            cursor.execute('insert into Cluster (Name) values (?)',[clusterName]);
            id = cursor.lastrowid
        else:
            id = r[0]
        self.conn.commit()
        cursor.close()
        return id
