#-*- coding: utf-8 -*-
from Context import Context
from Purchase import Purchase
from Repository import Repository



repo = Repository('database')
c = Context(repo)

c.addPurchase(Purchase(10, 'Batterien', 'Rewe', 'Visa'))
c.addPurchase(Purchase(20, 'Aepfel', 'Aldi', 'Bar'))
c.addPurchase(Purchase(30, 'Pudding', 'Penny', 'EC-Postbank'))
c.addClusterEntry('Rewe', 'Essen')
